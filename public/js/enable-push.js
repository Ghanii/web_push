// initSw() ;

// function initSw(){
//     if(!"serviceWorker" in navigator) {
//         console.log("no service worker for this navigator");
//         return ;
//     }

//     if( !"pushManager" in window ) {
//         console.log("push not supported");
//         return ;
        
//     }

//     navigator.serviceWorker.register("../sw.js")
//     .then( () => {
//         console.log("service worker installed");
//         initPush();
//     } )
//     .catch(err => {
//         console.log("service worker not installed" , err);
        
//     })
// }


// function initPush() {
//     if(!navigator.serviceWorker.ready) {
//         return ;
//     }

//     new Promise( function( resolve , rejecte ) {
//         const permissionRslt = Notification.requestPermission( function(rst) {
//             resolve(rst)
//         } );

//         if(permissionRslt) {
//             permissionRslt.then(resolve , rejecte);
//         }
//     }).then( (permissionRslt) => {
//         if( permissionRslt !== 'granted' ) {
//             throw new Error('no permission granted');
//         }
//         subscribeUser() ;
//     } );
// }

initSw() ;

function initSw(){

if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('../sw.js')
    .then( (reg) => {
      console.log('service worker registred' , reg);
      Notification.requestPermission(result =>  {
        console.log(result)
        if (result === 'granted') {
          navigator.serviceWorker.getRegistration().then(reg => {

            const subscriptionOptions = {
                userVisibleOnly: true ,
                applicationServerKey: 
                    "BJt2tbR9Ke_yz8EUInZwmB2Vq8I_-HTEYmYiGWd6yhGZbHHsEldqTyLWYaBFQ3IOdt40s0AdCSiuOa5STUqC_1U"
                
            };
    
             reg.pushManager.subscribe(subscriptionOptions)
            .then( (PushSubscription) => {
                console.log('revieved subscription' , JSON.stringify(PushSubscription));
        
                storePushSubscription(PushSubscription);
                
            } )

          })
          

        }
      });
      
    })
    .catch( (err) => {
      console.log('err not registred' , err);
      
    });
}
}


// function subscribeUser() {
    
//     navigator.serviceWorker.ready.then(  (registration) => {
//         const subscriptionOptions = {
//             userVisibleOnly: true ,
//             applicationServerKey: urlBase64ToUint8Array(
//                 "BJt2tbR9Ke_yz8EUInZwmB2Vq8I_-HTEYmYiGWd6yhGZbHHsEldqTyLWYaBFQ3IOdt40s0AdCSiuOa5STUqC_1U"
//             )
//         };

//         return registration.pushManager.subscribe(subscriptionOptions)
//     } )
//     .then( (PushSubscription) => {
//         console.log('revieved subscription' , JSON.stringify(PushSubscription));

//         storePushSubscription(PushSubscription);
        
//     } )
// }

function urlBase64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

function storePushSubscription(pushSubscription) {
   // const token = document.querySelector('meta[name=csrf-token]').getAttribute('content');

    fetch('/push', {
        method: 'POST',
        body: JSON.stringify(pushSubscription),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    })
        .then((res) => {
            return res.json();
        })
        .then((res) => {
            console.log(res)
        })
        .catch((err) => {
            console.log(err)
        });
}


navigator.serviceWorker.addEventListener('message' , evt => {
    console.log('message savage has been pushed!');
    
});

