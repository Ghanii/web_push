@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>

        <div class="mt-5">
            <button onclick="showNotif()"> show notif </button>


            <button  class="btn btn-primary ml-5"  onclick="sync()"> sync </button>
        </div>
   
       

<script> 
    function showNotif() {
        navigator.serviceWorker.ready.then( reg => {
            reg.showNotification('Vibration ', {
                     body: 'Notif ! for you! whya',
                    tag: 'vibration-sample',
                    icon : './notif.png' ,
                    actions : [
                         {action: 'like', title: '👍Like'},  
                         {action: 'reply', title: '⤻ Reply'},
                    ]
                    });
        });
    }


    function sync() {
        if(!navigator.onLine) {
            navigator.serviceWorker.ready.then( reg => {
                return reg.sync.register('firstSync' , {data : 'helo world'});
            } )
        }

        else {
            console.log('online now !');
            fetch('/sync', {
        method: 'GET',
        })
        .then((res) => {
            return res.json();
        })
        .then((res) => {
            console.log(res)
        })
        .catch((err) => {
            console.log(err)
        });

        }
        
    }
</script>

</div>
@endsection
